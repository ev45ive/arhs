import { Directive, ElementRef, OnDestroy, OnInit, Renderer, Input, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[highlight]',
  // host:{
  //   'mouseenter':'onEnter($event)'
  // }
})
export class HighlightDirective implements OnDestroy, OnInit {

  constructor(private elem:ElementRef,
              private renderer: Renderer) {  }
  
  @Input('highlight')
  color = 'red'

  active  = false

  @HostListener('mouseenter')
  onEnter(){
    this.active = true;
  }
  @HostListener('mouseleave')
  onExit(){
    this.active = false;    
  }

  @HostBinding('style.borderLeftColor')
  get getColor(){
    return this.active? this.color : ''
  } 

  ngOnInit(){
    // this.renderer.setElementStyle(this.elem.nativeElement,'borderLeftColor',this.color)
  }

  ngOnDestroy(){
    // 
  }

}
