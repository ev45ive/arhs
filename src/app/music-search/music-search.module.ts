import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { MusicFormComponent } from './music-form.component';
import { MusicAlbumsComponent } from './music-albums.component';
import { MusicAlbumComponent } from './music-album.component';
import { MusicService, URL} from './music.service'
import { ReactiveFormsModule } from '@angular/forms'
import { routing } from './music-search.routing'

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    routing
  ],
  declarations: [
    MusicSearchComponent, 
    MusicFormComponent, 
    MusicAlbumsComponent, 
    MusicAlbumComponent
  ],
  // exports:[
  //   MusicSearchComponent
  // ],
  providers:[
    MusicService,

    {
      provide:URL, useValue:'https://api.spotify.com/v1/search?type=album&market=PL&query='
    }
  ]
})
export class MusicSearchModule { }
