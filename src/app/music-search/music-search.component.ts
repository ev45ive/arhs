import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'music-search',
  template: `
    <div class="row">
      <div class="col">
        <div class="row">
          <div class="col">
            <music-form></music-form>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <music-albums></music-albums>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: []
})
export class MusicSearchComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
