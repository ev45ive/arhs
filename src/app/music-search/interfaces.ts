
// https://api.spotify.com/v1/search?type=album&market=FR&query=batman

export interface Entity{
    id: string;
    name: string;
}

export interface Album extends Entity {
    artists: Artist[]
    images: AlbumImage[]
}

export interface Artist  extends Entity{ }

export interface AlbumImage{
    url: string;
    height: number;
    width: number;
}


