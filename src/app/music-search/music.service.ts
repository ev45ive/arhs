import { Injectable, InjectionToken, Inject } from '@angular/core';
import { Album } from './interfaces'
import { Subject, Observable } from 'rxjs'
import { Http, URLSearchParams } from '@angular/http'

export const URL = new InjectionToken('URL');

@Injectable()
export class MusicService {

  albums$ = new Subject<Album[]>()
  
  queries$ = new Subject<string>()

  albums:Album[] = []

  getAlbums$(){
    return this.albums$.startWith(this.albums)
  }

  search(query){
    this.queries$.next(query)
  }


  constructor(private http:Http, @Inject(URL) private url) {

    this.albums$.subscribe(albums => {
      this.albums = albums
      localStorage.setItem('albums', JSON.stringify(albums))
    })

    this.queries$
    .do(query =>{
      localStorage.setItem('query', JSON.stringify(query))
    })
    .switchMap( query => this.http.get(this.url+query) ) 
    .map( response => response.json().albums.items)
    .subscribe( (albums:Album[]) => {
      this.albums$.next(albums)
    })


    let albums = JSON.parse(localStorage.getItem('albums'))
    if(albums && albums.length){
      this.albums$.next(albums)
    }

    //let URL = 'https://api.spotify.com/v1/search?type=album&market=PL&query='

  }

}
