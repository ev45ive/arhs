import { Component, OnInit, Input} from '@angular/core';
import { Album, AlbumImage } from './interfaces'

@Component({
  selector: 'music-album, [music-album]',
  template: `
    <img class="card-img-top" [src]="image.url">
    <div class="card-block">
      <h4 class="card-title">{{album.name}}</h4>
    </div>
  `,
  styles: [`
    :host(){
      min-width: 24% !important;
      flex-grow: 1 !important;
    }
  `]
})
export class MusicAlbumComponent implements OnInit {

  @Input('music-album')
  set onAlbum(album){
    this.album = album;
    this.image = this.album.images[1]
  }
  
  album:Album

  image:AlbumImage

  constructor() { }

  ngOnInit() {
  }

}
