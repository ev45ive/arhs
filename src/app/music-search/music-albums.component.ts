import { Component, OnInit } from '@angular/core';
import { Album } from './interfaces'
import { MusicService} from './music.service'

@Component({
  selector: 'music-albums',
  template: `
    <div class="card-group">
      <div *ngFor="let album of albums$ | async" class="card" [music-album]="album"></div>
    </div>
  `,
  styles: []
})
export class MusicAlbumsComponent implements OnInit {

  constructor(private service:MusicService) { }

  albums$

  ngOnInit() {
    this.albums$ = this.service.getAlbums$()
  }

}
