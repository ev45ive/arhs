import { Component, OnInit } from '@angular/core';
import { Album } from './interfaces'
import { MusicService} from './music.service'
import { FormGroup, FormControl, Validators, ValidatorFn, ValidationErrors,AbstractControl,AsyncValidatorFn} from '@angular/forms'

@Component({
  selector: 'music-form',
  template: `
    <form class="form" [formGroup]="queryForm">
      <div class="form-group">
        <input class="col form-control" placeholder="Search" formControlName="query"
         #query (keyup.enter)="doSearch(query.value)">
      </div>
      
      <div *ngIf="queryForm.pending"> Please wait, Checking... </div>

      <div *ngIf="queryForm.controls.query.dirty || queryForm.controls.query.touched">
        <div *ngIf="queryForm.controls.query.errors?.required"> Field is required! </div>
        <div *ngIf="queryForm.controls.query.errors?.minlength"> Minimal characters 
        {{queryForm.controls.query.errors.minlength.requiredLength}}! </div>
        <div *ngIf="queryForm.controls.query.errors?.not_text">
          Cannot be equal to {{queryForm.controls.query.errors?.not_text.text}}
        </div>
      </div>
    </form>
  `,
  styles: [`
    input.ng-dirty.ng-invalid,
    input.ng-touched.ng-invalid {
        border: 1px solid red;
    }
  `]
})
export class MusicFormComponent implements OnInit {

  queryForm: FormGroup

  constructor(private service:MusicService) {

    const validateNot = (text):ValidatorFn => (
      (control:AbstractControl):ValidationErrors => {
        return control.value === text? {
          'not_text':{ text }
        } : null;
      }
    );

    const asyncValidateNot = (text):AsyncValidatorFn => (
      (control:AbstractControl):Promise<ValidationErrors> => {
        return new Promise((resolve)=>{
          setTimeout(()=>{
            resolve(control.value === text? {
              'not_text':{ text }
            } : null)
          },2000)
        })
      }
    )

    this.queryForm = new FormGroup({
        'query': new FormControl('',[
          Validators.required,
          Validators.minLength(3),
          //validateNot('Batman')
        ],[
          asyncValidateNot('Batman')
        ])
    })
    console.log(this.queryForm)
    
    this.queryForm.get('query').statusChanges.combineLatest(
    this.queryForm.get('query').valueChanges,(status, value)=>(
      status == 'VALID'? value : ''
    ))
    .filter( query => query.length >= 3 )
    .distinctUntilChanged()
    .debounceTime(400)
    .subscribe(query => {
      this.doSearch(query)
    })

  }

  doSearch(query){
    console.log(query)
    this.service.search(query)
  }

  ngOnInit() {
  }

}
