import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'testing',
  template: `
    <p>{{value}}</p>
  `,
  styles: []
})
export class TestingComponent implements OnInit {

  value = 'pancakes'

  constructor() { }

  ngOnInit() {
  }

}
