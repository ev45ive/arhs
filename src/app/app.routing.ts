import { RouterModule, Routes } from '@angular/router'
import { PlaylistsComponent } from './playlists/playlists.component';
import { PlaylistDetailsComponent } from './playlists/playlist-details.component';

const routes:Routes = [
    { path:'', redirectTo: 'playlists', pathMatch: 'full'},
    { path:'music', loadChildren: './music-search/music-search.module#MusicSearchModule'},
    { path:'playlists', component: PlaylistsComponent, children:[
        { path:'show/:id', component: PlaylistDetailsComponent }
    ]},
    { path: '**', component: PlaylistsComponent }
]

export const routing = RouterModule.forRoot(routes,{
    enableTracing:true,
    //useHash: true
})