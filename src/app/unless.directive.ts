import { Directive, TemplateRef, ViewContainerRef, Input } from '@angular/core';

@Directive({
  selector: '[unless]'
})
export class UnlessDirective {

  @Input('unless')
  set onUnlessChange(hide){
    if(hide){
      this.vcr.clear()
    }else{
      this.vcr.createEmbeddedView(this.tpl)
    }
  }

  constructor(private tpl: TemplateRef<any>,
              private vcr: ViewContainerRef) {
    
  }

}
