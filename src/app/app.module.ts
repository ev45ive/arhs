import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, forwardRef, ExistingProvider} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { PlaylistListComponent } from './playlists/playlist-list.component';
import { PlaylistItemComponent } from './playlists/playlist-item.component';
import { PlaylistDetailsComponent } from './playlists/playlist-details.component';
import { HighlightDirective } from './highlight.directive';
import { UnlessDirective } from './unless.directive';
import { PlaylistsService} from './playlists/playlists.service'

// import { MusicSearchModule } from './music-search/music-search.module'
// import { URL } from './music-search/music.service'

import { routing } from './app.routing';
import { NavBarComponent } from './nav-bar.component';
import { TestingComponent } from './testing.component'

@NgModule({
  declarations: [
    AppComponent,
    PlaylistsComponent,
    PlaylistListComponent,
    PlaylistItemComponent,
    PlaylistDetailsComponent,
    HighlightDirective,
    UnlessDirective,
    NavBarComponent,
    TestingComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    //MusicSearchModule,
    routing 
  ],
  providers: [
    // {provide: PlaylistsService, useFactory:()=>{
    //     return new PlaylistsService(URL)
    // }, deps:[URL]},
    // { provide: AbstractService, useClass: PlaylistsService},

    PlaylistsService,

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }