import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Playlist } from './interfaces'

@Component({
  selector: '.playlist-item',
  template: `
  <div (click)="select()">
    {{playlist.name}}
    <ng-content></ng-content>
  </div>
  `,
  styles: [],
  // inputs:[
  //   'playlist:playlist'
  // ]
})
export class PlaylistItemComponent implements OnInit {

  @Input('data')
  playlist:Playlist

  @Output('selected')
  onSelect = new EventEmitter<Playlist>()

  select(){
    this.onSelect.emit(this.playlist)
  }
  
  constructor() { }

  ngOnInit() {
  }

}
