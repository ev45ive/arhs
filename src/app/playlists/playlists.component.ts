import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'playlists',
  template: `
    <div class="row">
      <div class="col">
        <playlist-list></playlist-list>
      </div>
      <div class="col">
         <router-outlet></router-outlet>
      </div>
    </div>
  `,
  styles: []
})
export class PlaylistsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
