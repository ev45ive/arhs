import { Injectable } from '@angular/core';
import { Playlist } from './interfaces'
import { InjectionToken, Inject } from '@angular/core'
import { Observable, Subject } from 'rxjs'

@Injectable()
export class PlaylistsService {

  playlists: Playlist[] = [
    {
      id: 1,
      name:'Much much Awesome', 
      color:'#00ff00',
      favourite: false
    },
    {
      id: 2,
      name: 'Angular greatest hits!',
      favourite: true,
      color:'#fff000'
    }
  ]

  selectedId: number = 1;

  selected$ = new Subject<Playlist>();

  playlists$ = new Subject<Playlist[]>();

  setSelected(id:number){
    this.selectedId = id
    this.selected$.next(this.getPlaylist(id))
  }

  getPlaylists$(){
    return this.playlists$
      .startWith(this.getPlaylists())
  }


  getSelected$(){
    return this.selected$
      .startWith(this.getPlaylist(this.selectedId))
  }

  getPlaylists(){
    return this.playlists;
  }

  getPlaylist(id){
    return this.playlists.find(
      playlist =>  playlist.id === id
    ) 
  }

  constructor() { }

}
