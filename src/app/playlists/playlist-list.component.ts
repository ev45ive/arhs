import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Playlist } from './interfaces'
import { PlaylistsService } from './playlists.service'
import { Router } from '@angular/router'


      // [style.borderLeftColor]="active == playlist? playlist.color : '' "
      // (mouseenter)="active = playlist"
      // (mouseleave)="active = false"

@Component({
  selector: 'playlist-list',
  template: `
    <ul class="list-group ">
      <li class="list-group-item playlist-item" 
      
      [data]= " playlist "
      (selected)=" select(playlist)"
      [class.active]=" selected == playlist"

      [routerLink]="['show',playlist.id]"

      [highlight]="playlist.color"

      *ngFor="let playlist of playlists$ | async" >

<input type="color" [(ngModel)]="playlist.color">

      </li>
    </ul>
  `,
  styles: [`
    li{
      border-left: 15px solid transparent
    }

    :host >>> .test_class{
      border: 1px solid black;
      display:block;
    }
  `]
})
export class PlaylistListComponent implements OnInit {

  selected:Playlist;

  select(playlist){
    this.selected = playlist
    this.service.setSelected(playlist.id)
    // or THAT:
    // this.router.navigate(['show',playlist.id])
  }

  playlists$;
  playlists: Playlist[] = []

  constructor(private service:PlaylistsService,
            private router: Router) { }

  ngOnInit() {
//   this.playlists = this.service.getPlaylists()
   this.playlists$ = this.service.getPlaylists$()
  }

}
