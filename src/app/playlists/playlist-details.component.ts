import { Component, OnInit, ViewChild,ElementRef,AfterViewInit } from '@angular/core';
import {Playlist} from './interfaces'
import { PlaylistsService } from './playlists.service'
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'playlist-details',
  template: `
  <ng-template #pleaseSelect>
    Please select a playlist!
  </ng-template>

    <div *ngIf=" selected$ | async as playlist;  else pleaseSelect "> 
    <button (click)=" edit = !edit "> {{ edit? 'Show' : 'Edit'}} </button>

      <div [ngSwitch]="edit">
        <div *ngSwitchDefault>
          <p> Name: {{playlist.name}} </p>
          <p> {{ playlist.favourite? 'Favourite': '' }} </p>
          <p [style.color]="playlist.color"> Color </p>
        </div>
        <form #playlistForm="ngForm" *ngSwitchCase="true">
          <div class="input-group">
            <label>Name</label>
            <input type="text" class="form-control" [ngModel]=" playlist.name" name="name" required>
          </div>
          <div class="input-group">
            <label>Favourite</label>
            <input type="checkbox"  [ngModel]=" playlist.favourite" name="favourite" >
          </div>
          <div class="input-group">
            <label>Color</label>
            <input type="color"  [ngModel]=" playlist.color" name="color">
          </div>
          <button (click)="playlistForm.valid && save(playlistForm.value)">Save</button>
        </form>
      </div>
    </div>
  `,
  styles: []
})
export class PlaylistDetailsComponent implements OnInit,AfterViewInit {

  @ViewChild('myButton')
  therebebutton:ElementRef

  ngAfterViewInit(){
    console.log(this.therebebutton)
  }

  playlist:Playlist

  save(){
    console.log(arguments)
  }

  constructor(private  service:PlaylistsService,
              private route: ActivatedRoute ) { }

  sub;

  selected$;

  ngOnInit() {
    //this.selected$ = this.service.getSelected$()

    // let id = parseInt(this.route.snapshot.params['id'])
    // this.playlist = this.service.getPlaylist(id)

    this.selected$ = this.route.params
      .map(params=> parseInt(params['id']))
      .do(test => console.log(test))
      .map(id => this.service.getPlaylist(id))
      

    // .map( playlist => {
    //   return Object.assign({}, playlist)
    // })

    // this.sub = this.service.getSelected$().subscribe( playlist =>
    //   this.playlist = playlist 
    // )
  }
  ngOnDestroy(){
    // this.sub.unsubscribe()
  }

}
