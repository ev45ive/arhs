export interface Playlist{
  id:number;
  name: string;
  favourite: boolean;
  /**
   * Color is not importatnt
   */
  color?: string;
}