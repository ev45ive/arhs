import { ArhsPage } from './app.po';

describe('arhs App', () => {
  let page: ArhsPage;

  beforeEach(() => {
    page = new ArhsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
